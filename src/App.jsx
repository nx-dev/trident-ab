import '@shopify/polaris/dist/styles.css';
import './App.scss';
import { AppProvider, Button, Frame, /* Icon, */ Page } from '@shopify/polaris';
import { Provider as AppBridgeProvider } from '@shopify/app-bridge-react';
import useToken from './hooks/useToken';
import { Provider as SessionProvider } from './context/main';
import TabMenu from './components/TabMenu';
// import { SettingsMajor } from '@shopify/polaris-icons';
import { Suspense } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import RouterComponent from './Router';
import enTranslations from '@shopify/polaris/locales/en.json';

const tabs = [
  {
    id: '/',
    content: 'Dashboard',
    type: 'link',
    hide: false,
  },
  // {
  //   id: "/plan",
  //   content: 'Plan & Billing',
  //   type: 'link',
  // },
  {
    id: "/help",
    content: 'Help',
    type: 'link',
  },
  {
    id: "/test",
    content: <Button primary>Create test</Button>,
    type: 'action',
  }
];

function App() {
  const { error, token, shopOrigin } = useToken();

  if (error) return <div>error</div>;

  if (!token) return <div>Loading...</div>;
  
  return (
    <div className="App">
      <SessionProvider token={token}>
        <AppProvider i18n={enTranslations}>
          <AppBridgeProvider config={{ apiKey: process.env.REACT_APP_API_KEY, shopOrigin, forceRedirect: !Boolean(process.env.REACT_APP_DEBUG_MODE) }}>
            <Router>
              <Frame>
                <TabMenu {...{tabs}}  />
                <Suspense fallback={<div>Loading</div>} >
                  <RouterComponent />
                </Suspense>
              </Frame>
            </Router>
          </AppBridgeProvider>
        </AppProvider>      
      </SessionProvider>
    </div>
  );
}

export default App;
