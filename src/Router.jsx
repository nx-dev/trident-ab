import { Page } from '@shopify/polaris';
import React, { useEffect } from 'react';
import { Switch, Route } from 'react-router-dom';
import Dashboard from './containers/Dashboard/Dashboard';
import Details from './containers/Details/Details';
// import PageCus from './containers/Page/Page';
import TestForm from './containers/TestForm/TestForm';
import { useGetShop } from './hooks/requests';


function RouterComponent(props) {
  const [getShop, { data: shop }] = useGetShop()
  useEffect(() => {
    getShop()
  }, [])

  return <Switch>
    <Route exact path="/">
      <Page>
        <Dashboard />
      </Page>
    </Route>
    <Route exact path="/details">
      <Page>
        <Details />
      </Page>
    </Route>
    <Route path="/help">
      {/* <Page {...props} /> */}
    </Route>
    <Route path="/test">
      <TestForm {...props} />
    </Route>
    
  </Switch>
}

export default RouterComponent;
