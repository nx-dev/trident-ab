import styles from './TabMenu.module.scss';
import classNames from 'classnames';
import useCustomHistory from '../hooks/useCustomHistory';


function TabMenu(props) {
  const {tabs} = props;
  const history = useCustomHistory();

  let selected = history.location.pathname;

  const findTab = tabs.find(_i => _i.id == selected && _i.type == 'link');
  

  if(!findTab){
    return null;
  }

  const onSelect = (path) => {
    history.push(path)
  }

  return (
    <div className={styles.TabMenu}>
      <div className={styles.links}>
        {tabs.filter(i => i.type === 'link' && !i.hide).map(tab => <div key={tab.id} onClick={() => onSelect(tab.id)} className={classNames(styles.link, {[styles.active]: selected === tab.id})}>{tab.content}</div>)}
      </div>
      <div className={styles.actions}>
        {tabs.filter(i => i.type === 'action').map(tab => <div key={tab.id} onClick={() => onSelect(tab.id)} className={classNames(styles.link, {[styles.active]: selected === tab.id})}>{tab.content}</div>)}
      </div>
    </div>
  );
}

export default TabMenu;
