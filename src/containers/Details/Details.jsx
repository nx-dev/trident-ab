import { Button, Card, DataTable, Icon } from '@shopify/polaris';
import { EditMinor, MobileBackArrowMajor } from '@shopify/polaris-icons';
import React from 'react'
import useCustomHistory from '../../hooks/useCustomHistory';
import './style.scss';

function Details() {
  const history = useCustomHistory();
  const rows = [
    ['Emerald Silk Gown', 101, 89, 32, 15, 34.3],
    ['Mauve Cashmere Scarf', 101, 89, 32, 15, 34.3],
  ];

  return (
    <div className="Details">
      <div className="header">
        <div className="block-left">
          <Button icon={<Icon source={MobileBackArrowMajor}/>} onClick={() => history.push('/')}></Button>
          Back to Dashboard
        </div>
        <div className="actions">
          <Button icon={<Icon source={EditMinor} />}>Edit</Button>
          <Button primary>Set the winner</Button>
        </div>
      </div>
      <div className="title">Balance Model 4 <div className="status completed"><img src="/status_completed.svg" />1 Completed</div></div>
      <Card>
        <DataTable
          columnContentTypes={[
            'text',
            'numeric',
            'numeric',
            'numeric',
            'numeric',
            'numeric',
          ]}
          headings={[
            'Variant',
            'Product page views',
            'Add to cart',
            'Units sold',
            'Revenues',
            'Revenue per views',
          ]}
          rows={rows}
        />
      </Card>
    </div>
  )
}

export default Details
