import { ActionList, Button, Card, DataTable, Filters, Icon, Popover, Select } from '@shopify/polaris'
import { MobileHorizontalDotsMajor } from '@shopify/polaris-icons';
import React, { useCallback, useState } from 'react'
import useCustomHistory from '../../hooks/useCustomHistory';
import './style.scss';


function Dashboard() {
  const history = useCustomHistory();
  const [search, setSearch] = useState('');
  const [sortedRows, setSortedRows] = useState(null);


  const activator = (
    <Button onClick={() => {}}>
      <Icon source={MobileHorizontalDotsMajor} />
    </Button>
  );


  const initiallySortedRows = [
    [
      <div className="productName">
        <div className="img"><img src="/logo192.png" /></div> Balance model 4
      </div>, 
      <div className="block">
        Nike Air Max 90, 124689
      </div>,
      <div className="block">
        <div className="status completed"><img src="/status_completed.svg" />1 Completed</div>
      </div>,
      <div className="block">
        0 minutes left
      </div>,
      <div className="actions">
        <Button primary onClick={() => {history.push('/details')}}>View</Button>
        <Popover
          active={false}
          activator={activator}
          onClose={() => {}}
        >
          <ActionList items={[{content: 'Import'}, {content: 'Export'}]} />
        </Popover>
      </div>
    ]
  ];
  const rows = sortedRows ? sortedRows : initiallySortedRows;


  const filters = [];

  const handleSort = useCallback(
    (index, direction) => setSortedRows(sortCurrency(rows, index, direction)),
    [rows],
  );

  const options = [
    {label: 'Today', value: 'today'},
    {label: 'Yesterday', value: 'yesterday'},
    {label: 'Last 7 days', value: 'lastWeek'},
  ];

  return (
    <div className="Dashboard">
      <Card>
        <Card.Section>
          <Filters
            queryValue={search}
            filters={filters}
            onQueryChange={setSearch}
            onQueryClear={setSearch}
          >
           <Select
              options={options}
              onChange={() => {}}
              value=""
            />
          </Filters>
        </Card.Section>
        <DataTable
          columnContentTypes={[ 
            'text',
            'text',
            'text',
            'text',
            'text',
          ]}
          headings={[
            'Name',
            'Product',
            'State',
            'Time',
            '',
          ]}
          rows={rows}
          sortable={[true, true, true, true, false]}
          defaultSortDirection="descending"
          initialSortColumnIndex={0}
          onSort={handleSort}
        />
      </Card>
    </div>
  )
}

function sortCurrency(rows, index, direction) {
  return [...rows].sort((rowA, rowB) => {
    const amountA = parseFloat(rowA[index].substring(1));
    const amountB = parseFloat(rowB[index].substring(1));

    return direction === 'descending' ? amountB - amountA : amountA - amountB;
  });
}

export default Dashboard
