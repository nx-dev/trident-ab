import { Button } from '@shopify/polaris';
import React, { useState } from 'react';
import FormStep1 from './components/FormStep1/FormStep1';
import FormStep2 from './components/FormStep2/FormStep2';
import FormStep3 from './components/FormStep3/FormStep3';
import Tab from './components/Tab/Tab';
import './style.scss';

function TestForm() {


  const [state, setState] = useState({
    tabActive: 'details',
    nextForm: 'create_variants',
    prevForm: '',
  });

  const nextStep = () => {
    const newState = {
      ...state,
      tabActive: state.nextForm
    }

    switch (newState.nextForm) {
      case 'create_variants':
        newState.nextForm = 'choose_period'
        newState.prevForm = 'details'
        break;
      case 'choose_period':
        newState.nextForm = 'choose_period';
        newState.prevForm = 'create_variants';
        break;
      default:
        break;
    }
    setState(newState)
  }

  const prevStep = () => {
    const newState = {
      ...state,
      tabActive: state.prevForm
    }

    switch (newState.prevForm) {
      case 'details':
        newState.nextForm = 'create_variants'
        newState.prevForm = ''
        break;
      case 'create_variants':
        newState.nextForm = 'choose_period';
        newState.prevForm = 'details';
        break;
      default:
        break;
    }

    setState(newState)
  }

  return (
    <div className="TestForm">
      <Tab selected={state.tabActive} />
      <div className="form-content">
        {state.tabActive == 'details' && <FormStep1 />}
        {state.tabActive == 'create_variants' && <FormStep2 />}
        {state.tabActive == 'choose_period' && <FormStep3 />}
      </div>
      <div className="form-footer">
        {state.prevForm ? <Button onClick={prevStep}>Previous step</Button> : <div></div>}
        <Button primary onClick={nextStep}>Next</Button>
      </div>
    </div>
  )
}

export default TestForm
