import React from 'react';
import './style.scss';
import classNames from 'classnames';

function Tab({selected}) {

  const tabs = [
    {
      title: 'Test Details',
      name: 'details'
    },
    {
      title: 'Create Variants',
      name: 'create_variants'
    },
    {
      title: 'Choose Period',
      name: 'choose_period'
    },
  ]

  return (
    <div className="Tab">
      {tabs.map((item, index) => <div key={item.name} className={classNames("item", {active: selected == item.name})}>{index + 1}. {item.title}</div>)}
    </div>
  )
}

export default Tab
