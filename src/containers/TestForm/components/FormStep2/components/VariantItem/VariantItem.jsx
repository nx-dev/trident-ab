import { Icon, Label, TextField, Link } from '@shopify/polaris';
import { CircleCancelMajor } from '@shopify/polaris-icons';
import React from 'react';
import './style.scss';

function VariantItem() {
  return (
    <div className="VariantItem">
      <div className="title">Variant: Grey</div>
      <div className="row">
        <div className="col-2">
          <TextField
            label="Quantity"
            type="number"
            value={''}
            onChange={() => {}}
          />
        </div>
        <div className="col-2">
          <TextField
            label="Quantity"
            type="number"
            value={''}
            onChange={() => {}}
          />
        </div>
      </div>
      <div className="photos">
        <Label>Photos</Label>
        <div className="photo-items">
          <div className="photo-item">
            <div className="close">
              <Icon source={CircleCancelMajor} />
            </div>
          </div>
          <div className="photo-item">
            <Link>Add new</Link>
          </div>
        </div>
      </div>
    </div>
  )
}

export default VariantItem
