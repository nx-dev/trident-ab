import { Icon, Label, Link } from '@shopify/polaris';
import { CircleCancelMajor } from '@shopify/polaris-icons';
import React from 'react'
import './style.scss';

function Photos() {
  return (
    <div className="Photos">
      <Label>Photos</Label>
      <div className="photo-items">
        <div className="photo-item">
          <div className="close">
            <Icon source={CircleCancelMajor} />
          </div>
        </div>
        <div className="photo-item">
          <div className="close">
            <Icon source={CircleCancelMajor} />
          </div>
        </div>
        <div className="photo-item">
          <Link>Add new</Link>
        </div>
      </div>
    </div>
  )
}

export default Photos
