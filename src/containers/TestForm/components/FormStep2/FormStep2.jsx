import { Button, Card, Icon, TextField } from '@shopify/polaris';
import { CircleInformationMajor } from '@shopify/polaris-icons';
import React from 'react'
import Photos from './components/Photos/Photos';
import VariantItem from './components/VariantItem/VariantItem';
import './style.scss';

function FormStep2() {
  return (
    <div className="FormStep2">
      <div className="title">Create variants for your A/B test</div>
      <Card>
        <Card.Section>
          <div className="product-item">
            <div className="product-content">
              <div className="img"><img src="/logo512.png" /></div>
              <div>
                <div className="title">New Balance 990v5</div>
                <div className="text">Description</div>
              </div>
            </div>
            <Button>Change product</Button>
          </div>
        </Card.Section>
      </Card>
      <div className="row Variant">
        <div className="col-2">
          <div className="title">Variant A <div><Icon source={CircleInformationMajor} /></div></div>
          <div className="text">Needed to compare the changes</div>
          <Card>
            <div className="disabled" />
            <Card.Section>
              <div className="m-b-20">
                <TextField label="Product name" placeholder="Product name" value={''} onChange={() => {}} />
              </div>
              <div className="m-b-20">
                <TextField multiline={4} value={''} onChange={() => {}} />
              </div>
              <Photos />
              <VariantItem />
              <VariantItem />
            </Card.Section>
          </Card>
        </div>
        <div className="col-2">
          <div className="title">Variant B <div><Icon source={CircleInformationMajor} /></div></div>
          <div className="text">You can change any field of this variant</div>
          <Card>
            <Card.Section>
              <div className="m-b-20">
                <TextField label="Product name" placeholder="Product name" value={''} onChange={() => {}} />
              </div>
              <div className="m-b-20">
                <TextField multiline={4} value={''} onChange={() => {}} />
              </div>
              <Photos />
              <VariantItem />
              <VariantItem />
            </Card.Section>
          </Card>
        </div>
      </div>
      <div className="block-info">
        <div className="video">
          <img src="/Thumbnail.png" />
        </div>
        <div className="text-content">
          <div className="title">Tailoring your online presence to connect directly with your consumers</div>
          <div className="text">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nascetur phasellus mi accumsan, dui tristique fermentum. Imperdiet massa porttitor leo, integer gravida eu quis sociis bibendum. Urna, cursus adipiscing sed cras consequat, accumsan duis at lorem. Sit volutpat nulla sit sit ut nulla. Sed egestas commodo consequat blandit enim tortor ipsum. Praesent amet nisl convallis turpis vitae mi.
          </div>
          <Button>Learn more</Button>
        </div>
      </div>
    </div>
  )
}

export default FormStep2
