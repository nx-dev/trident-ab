import { Button, Card, Link, TextField } from '@shopify/polaris'
import React from 'react'
import './style.scss';

function FormStep3() {
  return (
    <div className="FormStep3">
      <div className="text-content">
        <div className="title">Choose period for the test</div>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Est vel purus malesuada quam vitae aliquet purus proin augue. Sit quam vitae elementum lectus consectetur blandit. Ac tristique aliquet vitae, consectetur. Aliquet malesuada faucibus ut pellentesque dapibus.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Est vel purus malesuada quam vitae aliquet purus proin augue.</p>
      </div>
      <div className="form-content">
        <Card title="Adjust the timeframe" sectioned>
          <div className="row">
            <div className="col-2">
              <TextField label="Give a name to your test" value={''} onChange={() => {}} />
            </div>
            <div className="col-2">
              <TextField label="Give a name to your test" value={''} onChange={() => {}} />
            </div>
            <div className="col-2">
              <TextField label="Give a name to your test" value={''} onChange={() => {}} />
            </div>
            <div className="col-2">
              <TextField label="Give a name to your test" value={''} onChange={() => {}} />
            </div>
          </div>
        </Card>
        <Card>
          <Card.Section>
            <div className="row">
              <div className="col-2">
                <div className="Switches">
                  <div className="title">Switches between A & B</div>
                  <div className="text">48</div>
                </div>
              </div>
              <div className="col-2">
                <TextField label="Give a name to your test" value={''} onChange={() => {}} />
              </div>
            </div>
          </Card.Section>
        </Card>
      </div>
    </div>
  )
}

export default FormStep3
