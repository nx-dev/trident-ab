import { Button, Card, Link, TextField } from '@shopify/polaris'
import React from 'react'
import './style.scss';

function FormStep1() {
  return (
    <div className="FormStep1">
      <div className="text-content">
        <div className="title">Set up your A/B test</div>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Est vel purus malesuada quam vitae aliquet purus proin augue. Sit test instructions <Link>test instructions</Link> entum lectus consectetur blandit. Ac tristique aliquet vitae, consectetur. Aliquet malesuada faucibus ut pellentesque dapibus.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Est vel purus malesuada quam test instructions <Link>test instructions</Link> proin augue. Sit quam vitae elementum lectus consectetur blandit.</p>
      </div>
      <div className="form-content">
        <Card>
          <Card.Section>
            <TextField label="Give a name to your test" value={''} onChange={() => {}} />
          </Card.Section>
        </Card>
        <Card>
          <Card.Section>
            <div className="product-item">
              <div className="product-content">
                <div className="img"><img src="/logo512.png" /></div>
                <div>
                  <div className="title">New Balance 990v5</div>
                  <div className="text">Description</div>
                </div>
              </div>
              <Button>Change product</Button>
            </div>
          </Card.Section>
        </Card>
      </div>
    </div>
  )
}

export default FormStep1
