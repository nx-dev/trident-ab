const dotenv = require('dotenv');
dotenv.config();

module.exports = {
  "development": {
    "username": "postgres",
    "password": "postgres",
    "database": "priceup",
    "host": "127.0.0.1",
    "dialect": "postgres"
  },
  "test": {
    "username": "root",
    "password": null,
    "database": "database_test",
    "host": "127.0.0.1",
    "dialect": "postgres"
  },
  "staging": {
    "dialectOptions": {
      "ssl": {
        rejectUnauthorized: false,
      }
    },
    // "url": process.env.DATABASE_URL,
    "use_env_variable": "DATABASE_URL",
    "dialect": "postgres",
    "migrationStorageTableName": "priceup_meta",
  },
  "production": {
    "dialectOptions": {
      "ssl": {
        rejectUnauthorized: false,
      }
    },
    "use_env_variable": "DATABASE_URL",
    "dialect": "postgres"
  }
};
