require('isomorphic-fetch');
const express = require('express');
const app = express();
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser');
const { default: createShopifyAuth, verifyRequest } = require('koa-shopify-jwt-auth');
const { receiveWebhook, registerWebhook } = require('@shopify/koa-shopify-webhooks');
const dotenv = require('dotenv');
dotenv.config();
const Shopify = require('shopify-api-node');


const tokens = {};
async function storeToken(shop, token) {
  tokens[shop] = token;
}
async function getToken(shop) {
  return typeof tokens[shop] === 'undefined' ? null : tokens[shop];
}

(async () => {
  // const shops = await db.Shop.findAll({});
  // shops.forEach(shop => {
  //   storeToken(shop.shopify_domain, shop.access_token);
  // });
  if (process.env.DEBUG_SHOPIFY_DOMAIN && process.env.DEBUG_ACCESS_TOKEN) {
    tokens[ process.env.DEBUG_SHOPIFY_DOMAIN ] = process.env.DEBUG_ACCESS_TOKEN;
  }
})();

const {
  SHOPIFY_API_SECRET_KEY,
  SHOPIFY_API_KEY,
  SERVER_PORT,
  PORT,
  TUNNEL_URL,
} = process.env;

function koaMiddlewareWrapper(middleware, override = {}) {
  return (req, res, next) => {
    return middleware({
      req,
      res,
      path: req.path,
      query: req.query,
      host: req.host,
      cookies: {
        get: (name) => {
          return req.cookies[name];
        },
        set: (name, value, options) => {
          res.cookie(name, value, options); 
        },
      },
      headers: req.headers,
      redirect: (url) => res.redirect(url),
      set session(value) {
        req.session = value;
      },
      get session() {
        return req.session;
      },
      set status(value) {
        res.status(value);
      },
      set body(value) {
        if (typeof value === 'string') {
          res.set('Content-Type', 'text/html');
          res.send(value);
        } else {
          res.set('Content-Type', 'application/json');
          res.send(JSON.stringify(value));
        }
      },
      ...override,
    }, next);
  }
}

app.use(cookieParser());
app.use(bodyParser.json());

app.use(koaMiddlewareWrapper(createShopifyAuth({
  apiKey: SHOPIFY_API_KEY,
  secret: SHOPIFY_API_SECRET_KEY,
  scopes: ['read_products'], // , 'read_analytics'
  async afterAuth(ctx) {
    const { shop: shopOrigin, accessToken } = ctx.session;
    console.log('shop, accessToken', shopOrigin, accessToken);
    storeToken(shopOrigin, accessToken);
    // try {
    //   const shop = await db.Shop.findOne({shopify_domain: shopOrigin});
    //   if (shop) {
    //     await db.Shop.update({access_token: accessToken}, {where: {id: shop.id}});
    //   } else {
    //     await db.Shop.create({shopify_domain: shopOrigin, access_token: accessToken});
    //   }
    // } catch (err) {
    //   console.log('TCL: afterAuth -> err', err);      
    // }

    const registration = await registerWebhook({
      address: `${TUNNEL_URL}/webhooks/app/uninstalled`,
      topic: 'APP_UNINSTALLED',
      accessToken,
      shop: shopOrigin,
      apiVersion: '2020-07',
    });

    if (registration.success) {
      console.log('Successfully registered webhook!');
    } else {
      console.log('Failed to register webhook', registration.result);

    }
    // await getSubscriptionUrl(ctx, accessToken, shop);

    ctx.redirect(`https://${shopOrigin}/admin/apps/${SHOPIFY_API_KEY}`);
  },
})));


app.use((req, res, next) => {
  if (process.env.DEBUG_SHOPIFY_DOMAIN && process.env.DEBUG_ACCESS_TOKEN) {
    req.session = {
      shop: process.env.DEBUG_SHOPIFY_DOMAIN,
      accessToken: process.env.DEBUG_ACCESS_TOKEN,
    };
    return next();
  }
  if (req.path.indexOf('/api') !== 0) {
    return next();
  }
  koaMiddlewareWrapper(verifyRequest({
    secret: SHOPIFY_API_SECRET_KEY,
    getOfflineToken: getToken, // if the function returns null it will be redirected to auth flow
  }), { path: req.path === '/api/verify_token' ? '/verify_token' : req.path })(req, res, next);
});

app.get('/api/dashboard', async (req, res) => {
 
  res.json({
    price_up_total_earnings: 2510.99,
    price_up_active_products: 300,
    link_app: ''
  }); 
});


app.use(express.static('build'));

app.use(function (err, req, res, next) {
  console.error(err.stack)
  res.status(500).send('Something broke!')
})

app.listen(SERVER_PORT || PORT || 4001);

// v2